# complaint management system

Web based Complaint Management System using native PHP and Mysql

1. Install Composer 
    https://getcomposer.org/
2. Run command in project folder from terminal
    composer install
3. Create DB    
4. Copy inc-db.example.php to inc-db.php
5. Setup db setting in inc-db.php
4. Run command in project folder from terminal
    windows : vendor\bin\phinx migrate -e development 
    linux : vendor/bin/phinx migrate -e development