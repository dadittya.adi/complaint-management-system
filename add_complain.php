<?php 
	//$requiredLevel = array("SUPERADMIN");
	include "inc-header.php";
	
	if($loginType=='SUPERADMIN') $txt_readonly = '';
	else $txt_readonly = 'readonly="readonly"';
?>

<body class="expand-data panel-data">
    
	<?php include "inc-top-bar.php"; ?>

    <?php include "inc-main-nav.php"; ?>

    <div class="pg-tp">
        <i class="fa fa-commenting-o"></i>
        <div class="pr-tp-inr">
            <h4>Input Komplain</h4>
            <span class="my_breadcrumb">
				Input komplain customer
			</span>
        </div>
    </div>
    <!-- Page Top -->

    <div class="panel-content">
        <div class="filter-items">
            <div class="row grid-wrap">
			
				<div class="widget pad10"> 		
					<div class="col-md-12 col-sm-12 col-lg-12">
						<span id="mainNotification"></span>
					</div>
					
					<div class="col-md-8 col-sm-12 col-lg-8">
						<div class="form-wrp">
							<div class="row form-group">
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label>Kategori</label>
								<div class="slct-bx"><span>
									<select name="id_category" id="id_category">
										<?php 
											$query  = 'select * from tbl_complain_category order by id ASC';
											$result = mysqli_query($mysql_connection, $query);
											while($data = mysqli_fetch_array($result)) {
												echo '<option value="'.$data['id'].'">'.$data['complain_category_name'].'</option>';
											}
										?>
									</select>
								</span></div>
							  </div>
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label>Nama Customer</label>
								<input type="text" <?php echo $txt_readonly; ?> name="customer_name" placeholder="Nama Customer" class="form-control" id="customer_name" value="<?php echo $fullname; ?>">
							  </div>
							</div>
							<div class="row form-group">
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label for="email">Email</label>
								<input type="text" <?php echo $txt_readonly; ?> name="email" placeholder="Email" class="form-control" id="email" value="<?php echo $email; ?>">
							  </div>
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label>Prioritas</label>
								<div class="slct-bx"><span>
									<select name="priority" id="priority">
										<option value="1">HIGH</option>
										<option value="2">MEDIUM</option>
										<option value="3">LOW</option>
									</select>
								</span></div>
							  </div>
							</div>
							<div class="row form-group">
							  <div class="col-md-12 col-sm-12 col-lg-12">
								<label>Judul Komplain</label>
								<input type="text" name="title" placeholder="Judul Komplain" class="form-control" id="title">
							  </div>
							</div>
							<div class="row form-group">
							  <div class="col-md-12 col-sm-12 col-lg-12">
								<label for="message">Isi Komplain</label>
								<textarea name="message" placeholder="Isi Komplain" class="form-control" id="message"></textarea>
							  </div>
							</div>
							<div class="row form-group">
								<div class="col-md-6 col-sm-12 col-lg-6">
									<div class="form-wrp">
										<div class="row form-group">
										  <div class="col-md-12 col-sm-12 col-lg-12">
											<button class="brd-rd5 btn btn-sm btn-primary" onclick="addImages(1);"><i class="fa fa-plus"></i> Tambah Gambar</button>
										  </div>
										</div>
									</div>
								</div>
							</div>
							
							<span id="spanImagesTable"></span>
							
							<div class="row form-group">
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<a href="#" title="" class="brd-rd5 btn btn-sm btn-success" onclick="saveData();"><i class="fa fa-save"></i> Simpan</a>
								<a href="complain" title="" class="brd-rd5 btn btn-sm btn-danger"><i class="fa fa-remove"></i> Batal</a>
							  </div>
							</div>
						</div>
					</div>
				</div>
            </div>
            <!-- Filter Items -->
        </div>
    </div>
    <!-- Panel Content -->
	
	<?php include "custom_loading.php"; ?>
	
    <?php include "inc-footer.php"; ?>

    <!-- Vendor: Javascripts -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- Vendor: Followed by our custom Javascripts -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/select2.min.js" type="text/javascript"></script>
    <script src="js/slick.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
	
	<script src="js/inc-function.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		var rowNumber = 0;
		var rowCount  = 0;
		
		function addImages() {
			
			rowNumber++;
			rowCount++;
			
			var spanImagesRowName 		= 'spanImagesRow_'+rowNumber;
			var imageViewName 			= 'imageView_'+rowNumber;
			var complainImageName 		= 'complainImage_'+rowNumber;
			
			var input = "<span id='"+ spanImagesRowName +"'>"+
						"<div class='row form-group'> "+
						"<div class='col-md-12'>"+
						"<table width='100%' class='table'>"+
							"<tr>"+
								"<td width='150px' align='center'>"+
									"<img id='"+ imageViewName +"' src='img/default.jpg' style='width: 90px;' alt='Gambar'/>"+
								"</td>"+
								"<td>"+
									"<input type='file' name='"+ complainImageName +"' id='"+ complainImageName +"' accept='image/*' onchange='readURL(this, \"#"+ imageViewName +"\")'/>"+
								"</td>"+
								"<td>"+
									"<button class='brd-rd5 btn btn-sm btn-danger' onclick='removeImages("+ rowNumber +");'>X</button>"+
								"</td>"+
							"</tr>"+
						"</table>"+
						"</div>"+
						"</div>"+
						"</span>";
						
			$('#spanImagesTable').append(input);
		}
		
		function removeImages(rowNum) {
			
			if (confirm('Hapus gambar?')) {
				
				$('#spanImagesRow_'+rowNum).html('');
				rowCount--;
			}
		}
		
		function readURL(input, view) { 
			
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result); 
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

        function saveData() {
			
			$body.addClass("loadingClass");

    		var form_data = new FormData();
          	form_data.append('id_category', $('select[name=id_category]').val());
			form_data.append('customer_name', $('input[name=customer_name]').val());
			form_data.append('email', $('input[name=email]').val());
			form_data.append('title', $('input[name=title]').val());
			form_data.append('priority', $('select[name=priority]').val());
			form_data.append('message', $('textarea[name=message]').val());
			form_data.append('images_row_count', rowCount);
			
			for(var i=0; i<rowCount; i++) {
				
				var n = i+1;
				var complainImageName = 'complainImage_'+n;				
				form_data.append(complainImageName, $('#'+complainImageName).prop('files')[0]);
			}
			
			$.ajax({
				type: "POST",
                url: 'ajax_add_complain.php',
                processData: false,
                contentType: false,
                data: form_data
			}).done(function(data) {

				data = $.trim(data); 
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data komplain sudah disimpan"));
					resetdata();
					setTimeout(clearnotif, 5000);

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} else if (data == 'incorrect_file_type') {
					$('#mainNotification').html(showNotification("error", "File gambar harus berupa file jpg atau png atau gif"));
				} else if (data == 'invalid_email_format') {
					$('#mainNotification').html(showNotification("error", "Format email tidak benar"));
				}

				$('html, body').animate({scrollTop: '0px'}, 0);
			});
		
		}
		
		function resetdata() {
            $("#id_category").val($("#id_category option:first").val());
			$('#id_category').val($("#id_category option:first").val()).trigger('change.select2');
			$("#customer_name").val(<?php echo "\"$fullname\"";?>);
			$("#email").val(<?php echo "\"$email\"";?>);
			$("#title").val("");
			$("#priority").val($("#priority option:first").val());
			$('#priority').val($("#priority option:first").val()).trigger('change.select2');
			$("#message").val("");
			rowNumber = 0;
			rowCount  = 0;
			$('#spanImagesTable').html("");
        }
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
	</script>
</body>

</html>