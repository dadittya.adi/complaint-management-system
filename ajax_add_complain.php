<?php
	
	//$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";
	
	$user_id = $_SESSION['userID'];
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/complain_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'css_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'css_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

			} else {
				$response = "";
			}
		}

		return $response;
	}
	
	$id_category 		= sanitize_int($_REQUEST["id_category"]);
	$customer_name		= sanitize_sql_string($_REQUEST["customer_name"]);
	$email				= sanitize_sql_string($_REQUEST["email"]);
	$title				= sanitize_sql_string($_REQUEST["title"]);
	$priority			= sanitize_sql_string($_REQUEST["priority"]);
	$message			= sanitize_sql_string($_REQUEST["message"]);
	$images_row_count	= sanitize_int($_REQUEST["images_row_count"]);
	
	$message = str_replace("\r\n","<br />",$message );
	
	// if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	// 	echo "invalid_email_format";
	// 	exit;
	// }
	
	if ($id_category <> '0' &&  $message <> '' ) {
		
		usleep(rand(1000,1000000));
		
		// hitung jumlah komplain yang sudah ada
		$startDate = date("Y-m-d")." 00:00:00";
		$endDate   = date("Y-m-d")." 23:59:59";
		$query = "select count(id_complain) as jml from tbl_complain where submitted_date>='$startDate' and submitted_date<='$endDate'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$jml   = str_pad( $data['jml']+1, 3, '0', STR_PAD_LEFT);
		
		$ticket_number = date("dmy").$jml;

		$now = date("Y-m-d H:i:s");
			
		$query = "INSERT INTO tbl_complain(ticket_number, id_category, 
				  customer_name, email, title, message, 
				  status, user_id, submitted_date, priority) 
				  VALUES ('$ticket_number', '$id_category', 
				  '$customer_name', '$email', '$title', '$message',
				  'SUBMITTED', '$user_id', '$now', '$priority')";
        mysqli_query($mysql_connection, $query);
		
		//ambil id_complain terbaru
		$queryCheck		= "SELECT id_complain from tbl_complain WHERE ticket_number='$ticket_number' ";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		$dataCheck		= mysqli_fetch_array($resultCheck);
		$idNew			= $dataCheck['id_complain'];
		
		for($i=0; $i<$images_row_count; $i++) {
			
			$n = $i+1;
			$complainImageName = 'complainImage_'.$n;
			
			$imageFileName = "";
			if (!empty($_FILES[$complainImageName])) {
				$imageFileName = uploadImage($_FILES[$complainImageName]);
			} else {
				$imageFileName = "";
			}
			
			if($imageFileName<>'') {
				$query = "	INSERT INTO tbl_complain_image(id_complain, image)
							VALUES ('$idNew', '$imageFileName')";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		//TO-DO : kirim email notifikasi
		
        echo 'success'; 
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>