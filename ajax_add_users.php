<?php
	$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";

	$operator_name 				= sanitize_sql_string($_REQUEST["operator_name"]);
	$operator_email				= sanitize_sql_string($_REQUEST["operator_email"]);
	$operator_level				= sanitize_sql_string($_REQUEST["operator_level"]);
	$operator_password			= sanitize_sql_string($_REQUEST['operator_password']);
	$operator_passwordConfirm	= sanitize_sql_string($_REQUEST['operator_passwordConfirm']);
	$view_all					= sanitize_sql_string($_REQUEST["view_all"]);
	$operator_status			= sanitize_sql_string($_REQUEST["operator_status"]);
	$email_notification			= sanitize_sql_string($_REQUEST["email_notification"]);
	
	if (!filter_var($operator_email, FILTER_VALIDATE_EMAIL)) {
		echo "invalid_email_format";
		exit;
	}

	if ($operator_name <> '' &&  $email <> '' && $operator_password <> '' 
	    && $view_all <> '' && $operator_status <> '') {
		
		if($operator_password <> $operator_passwordConfirm) {
			echo "password_not_match";
			exit;
		}
		
		$queryCheck		= "SELECT user_id from tbl_user WHERE email='$operator_email'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "email_exist";
			exit;
		}
	
		$operator_password = md5($salt.$operator_password);
	
        $query = "INSERT INTO tbl_user(fullname, email, password, level, 
				  status, view_all, email_notification) 
				  VALUES ('$operator_name','$operator_email','$operator_password','$operator_level', 
				  '$operator_status', '$view_all', '$email_notification')";
        mysqli_query($mysql_connection, $query);
		
        echo 'success'; 
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>