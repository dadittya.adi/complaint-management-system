<?php
	//$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";
	include "inc-functions.php";

	$id_complain = sanitize_int($_REQUEST["id_complain"]);
	
	if ($id_complain <> 0) {
		
		$dataInUse = false;	 
		
		//cek dulu apakah sudah solved
		$queryCheck		= "SELECT status from tbl_complain WHERE id_complain='$id_complain' and status='SUBMITTED' ";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) == 0) {
			$dataInUse = true;	 
		}
			
		if($dataInUse) {
			echo "in_use";
			exit;
		}
		else {
			
			//hapus gambar
			$query 	= "select image from tbl_complain_image where id_complain='$id_complain'";
			$result = mysqli_query($mysql_connection, $query);
			while($data	= mysqli_fetch_array($result)) {
				
				@unlink('user_files/complain_image/'.$data['image']);
			
			}
			
			$query = "DELETE FROM tbl_complain_image WHERE id_complain='$id_complain'";
			mysqli_query($mysql_connection, $query);
			
			$query = "DELETE FROM tbl_complain WHERE id_complain='$id_complain'";
			mysqli_query($mysql_connection, $query);
		}
		echo 'success';
		exit;
	} else {
		echo "empty";
		exit;
	}
?>