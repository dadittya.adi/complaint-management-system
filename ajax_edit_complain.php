<?php
	
	//$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";
	
	$user_id = $_SESSION['userID'];
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/complain_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'css_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'css_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

			} else {
				$response = "";
			}
		}

		return $response;
	}
	
	$id_complain		= sanitize_int($_REQUEST["id_complain"]);
	$id_category 		= sanitize_int($_REQUEST["id_category"]);
	$customer_name		= sanitize_sql_string($_REQUEST["customer_name"]);
	$email				= sanitize_sql_string($_REQUEST["email"]);
	$title				= sanitize_sql_string($_REQUEST["title"]);
	$priority			= sanitize_sql_string($_REQUEST["priority"]);
	$message			= sanitize_sql_string($_REQUEST["message"]);
	$images_row_count	= sanitize_int($_REQUEST["images_row_count"]);
	
	$message = str_replace("\r\n","<br />",$message );
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		echo "invalid_email_format";
		exit;
	}
	
	if ($id_category <> '0' &&  $customer_name <> '' &&  $message <> '' ) {
			
		$query = "update tbl_complain set id_category='$id_category', 
				  customer_name='$customer_name', email='$email', 
				  title='$title', message='$message', priority='$priority'  
				  where id_complain='$id_complain' ";
        mysqli_query($mysql_connection, $query);
		
		for($i=0; $i<$images_row_count; $i++) {
			
			$n = $i+1;
			$complainImageName 	= 'complainImage_'.$n;
			$imagesIDName		= 'images_id_'.$n;
			$imagesTypeName 	= 'images_type_'.$n;
			
			$imageFileName = "";
			if (!empty($_FILES[$complainImageName])) {
				$imageFileName = uploadImage($_FILES[$complainImageName]);
			} else {
				$imageFileName = "";
			}
			
			if($imageFileName<>'') {
			
				$id_image	= sanitize_int($_REQUEST[$imagesIDName]);
				$image_type	= sanitize_sql_string($_REQUEST[$imagesTypeName]);
				
				if($image_type=='NEW') {
					$query = "	INSERT INTO tbl_complain_image(id_complain, image) VALUES ('$id_complain', '$imageFileName') ";
				}
				else {
					
					//hapus gambar yang lama
					$query 	= "select image from tbl_complain_image where id='$id_image'";
					$result = mysqli_query($mysql_connection, $query);
					$data 	= mysqli_fetch_array($result);
					if($data['image'] <> '') {
						@unlink('user_files/complain_image/'.$data['image']);
					}
					
					$query = "	UPDATE tbl_complain_image SET image='$imageFileName' where id='$id_image' ";
				}
					
				if($imageFileName<>'') {
					mysqli_query($mysql_connection, $query);
				}
			
			}
		}
		
        echo 'success'; 
		exit;  
		
	} else {
		echo "empty";
		exit;
	}
?>