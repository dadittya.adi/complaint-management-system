<?php
	$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";

	$operator_id				= sanitize_sql_string($_REQUEST["operator_id"]);
	$operator_name 				= sanitize_sql_string($_REQUEST["operator_name"]);
	$operator_email				= sanitize_sql_string($_REQUEST["operator_email"]);
	$operator_level				= sanitize_sql_string($_REQUEST["operator_level"]);
	$operator_password			= sanitize_sql_string($_REQUEST['operator_password']);
	$operator_passwordConfirm	= sanitize_sql_string($_REQUEST['operator_passwordConfirm']);
	$view_all					= sanitize_sql_string($_REQUEST["view_all"]);
	$operator_status			= sanitize_sql_string($_REQUEST["operator_status"]);
	$email_notification			= sanitize_sql_string($_REQUEST["email_notification"]);
	
	if (!filter_var($operator_email, FILTER_VALIDATE_EMAIL)) {
		echo "invalid_email_format";
		exit;
	}

	if ($operator_id <> 0 && $operator_name <> '' &&  $email <> '' && $view_all <> '' && $operator_status <> '') {
		
		if(($operator_password<>"") && ($operator_password <> $operator_passwordConfirm)) {
			echo "password_not_match";
			exit;
		}
		
		$queryCheck		= "SELECT user_id from tbl_user WHERE email='$operator_email' and user_id<>'$operator_id'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "email_exist";
			exit;
		}
	
        $query = "UPDATE tbl_user SET fullname='$operator_name', 
				  email='$operator_email', view_all='$view_all', 
				  status='$operator_status', email_notification='$email_notification',
				  level='$operator_level' ";
		if($operator_password <> '') {
			$operator_password = md5($operator_password);
			$query = $query.",password='$operator_password' ";	
		} 
		$query = $query." where user_id='$operator_id'";
        mysqli_query($mysql_connection, $query);
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>