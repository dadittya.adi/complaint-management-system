<?php
	include "check-admin-session.php";
	
	$userID	= $_SESSION['userID'];

	$type = sanitize_sql_string($_REQUEST["type"]);
	$id	  = sanitize_int($_REQUEST["id"]);

	if($type=='adm') {
		$tableName = 'tbl_user';
		$columnName = 'user_id';
	}
	else if($type=='kategori') {
		$tableName = 'tbl_complain_category';
		$columnName = 'id';
	}
	else if($type=='cmp') {
		$tableName = 'tbl_complain';
		$columnName = 'id_complain';
	}	
	
	if($type<>'cmp') {
		$query 	= "select * from ".$tableName."
				   where ".$columnName."='".$id."'";
	}
	else {
		$query 	= "select a.*,b.complain_category_name, c.fullname as processed_by_name, d.fullname as solved_by_name  
				   from tbl_complain a 
				   left join tbl_complain_category b on a.id_category=b.id 
				   left join tbl_user c on a.processed_by=c.user_id  
				   left join tbl_user d on a.solved_by=d.user_id  
				   where id_complain='$id' ";
	}
			   
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	if($type=='cmp') {
		
		$message = $data['message'];
		$message = str_replace("<br />", "\r\n", $message);
		$data['message'] = $message;
		
		$submitted_date = $data['submitted_date'];
		$submitted_date = date('d-m-Y H:i:s', strtotime($submitted_date));
		$data['submitted_date'] = $submitted_date;
		
		if($data['on_process_date']<>'') {
			$on_process_date = $data['on_process_date'];
			$on_process_date = date('d-m-Y H:i:s', strtotime($on_process_date));
			$data['on_process_date'] = $on_process_date;
		}
		
		if($data['solved_date']<>'') {
			$solved_date = $data['solved_date'];
			$solved_date = date('d-m-Y H:i:s', strtotime($solved_date));
			$data['solved_date'] = $solved_date;
		}
		
		//update statusnya menjadi on process
		$loginType= $_SESSION['loginType'];
		
		if($loginType == 'SUPERADMIN') {
			if($data['status'] == 'SUBMITTED' && $data['on_process_date'] == '') {
			
				$now = date("Y-m-d H:i:s");
				
				$queryUpdate = "update tbl_complain set status='ON PROCESS', on_process_date='$now', processed_by='$userID' 
								where id_complain='$id'";
				mysqli_query($mysql_connection, $queryUpdate);
			}
		}
	}
	
	echo json_encode($data);
?>
