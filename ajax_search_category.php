<?php
	$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";
	
	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from tbl_complain_category
					   where complain_category_name like '%$keyword%'";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from tbl_complain_category
			  where complain_category_name like '%$keyword%' 
			  order by id ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query); 

	echo "<table class='table table-striped table-hover'>
			<thead>
			  <tr>
				  <th width='2%'>No</th>
				  <th>Nama Kategori</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	
			</thead>
			<tbody>
			";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['complain_category_name'] . '</td>
				  <td align="center">
							  <a href="#modal" onclick="getedit(' . $data['id'] . ')">EDIT</a> | 
							  <a href="#" onclick="deleteKategori(' . $data['id'] . ',\'' . $data['complain_category_name'] . '\')">HAPUS</a>
							  </td>
				  </tr>';
		$i++;
	}

	echo "</tbody></table><br>";
	
	include "inc-paging.php";
?>