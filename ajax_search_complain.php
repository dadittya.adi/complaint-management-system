<?php
	//$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";
	
	$user_id = $_SESSION['userID'];
	
	//cek apakah bisa melihat semua data komplain
	//jika tidak bisa maka hanya bisa melihat yang dia input saja
	$query	= "select view_all from tbl_user where user_id='$user_id' ";
    $result = mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$view_all = $data['view_all'];
	
	$searchDate = sanitize_sql_string($_REQUEST["searchDate"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		
		//dicari 30 hari ke belakang
		$startDate 	= date('Y-m-d', strtotime('-30 days'))." 00:00:00";
		$endDate 	= date("Y-m-d")." 23:59:59";
	}
	
	$src_category	= sanitize_int($_REQUEST["src_category"]);
	$keyword		= sanitize_sql_string($_REQUEST["keyword"]);
	$src_status		= sanitize_sql_string($_REQUEST["src_status"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(a.id_complain) as num
					   from tbl_complain a 
					   left join tbl_complain_category b on a.id_category=b.id 
					   where (ticket_number like '%$keyword%' or customer_name like '%$keyword%'
					   or email like '%$keyword%' or phone like '%$keyword%') 
					   and submitted_date>='$startDate' and submitted_date<='$endDate' ";
	if($src_category <> '0') $query = $query." and id_category='$src_category' ";
	if($src_status <> '') $query = $query." and status='$src_status' ";
	if($view_all == 'N') $query = $query." and user_id='$user_id' ";
	
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select a.*, b.complain_category_name  
			  from tbl_complain a 
			  left join tbl_complain_category b on a.id_category=b.id 
			  where (ticket_number like '%$keyword%' or customer_name like '%$keyword%'
			  or email like '%$keyword%' or phone like '%$keyword%') 
			  and submitted_date>='$startDate' and submitted_date<='$endDate'  ";
	if($src_category <> '0') $query = $query." and id_category='$src_category' ";
	if($src_status <> '') $query = $query." and status='$src_status' ";
	if($view_all == 'N') $query = $query." and user_id='$user_id' ";
	
	$query = $query." order by priority, submitted_date ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query); 

	echo "<table class='table table-striped table-hover'>
			<thead>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='8%'>Tanggal</th>
				  <th width='10%'>Nomor Tiket</th>
				  <th width='3%'></th>
				  <th width='3%'>Prioritas</th>
				  <th width='10%'>Kategori</th>
				  <th width='10%'>Judul</th>
				  <th width='20%'>Customer</th>
				  <th width='10%'>Status</th>
				  <th width='10%'>&nbsp;</th>
				</tr>	
			</thead>
			<tbody>
			";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		$status = $data['status'];
		
		if($status == 'SUBMITTED') {
			$status = "<b><font color='red'>".$status."</font></b>";
			$btn_color = "btn-danger";
		}
		else if($status == 'ON PROCESS') {
			$status = "<b><font color='blue'>".$status."</font></b>";
			$btn_color = "btn-info";
		}
		else if($status == 'SOLVED') {
			$status = "<b><font color='green'>".$status."</font></b>";
			$btn_color = "btn-success";
		}
		
		if($data['priority']=='1') $priority = '<img src="img/high.png" style="width:40px;">';
		else if($data['priority']=='2') $priority = '<img src="img/med.png" style="width:40px;">';
		else if($data['priority']=='3') $priority = '<img src="img/low.png" style="width:40px;">';
		
		//cek apakah ada gambar
		$queryImg 	= "select image from tbl_complain_image where id_complain='".$data['id_complain']."' "; 
		$resultImg  = mysqli_query($mysql_connection, $queryImg);	
		if(mysqli_num_rows($resultImg)) {
			$attachment = '<a href="#modalMedia" onclick="getImages(' . $data['id_complain'] . ')">
						   <i class="fa fa-paperclip" style="font-size:18px;"></i> 
						   </a>';
		}
		else {
			$attachment = '';
		}

		echo '<tr>
				  <td>'.$i.'</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data['submitted_date'])) . '</td>
				  <td> 
					<a class="brd-rd5 btn btn-sm '.$btn_color.'" href="#modal" onclick="getedit(' . $data['id_complain'] . ')">' . $data['ticket_number'] . '</a>
				  </td>
				  <td>'.$attachment.'</td>
				  <td>'.$priority.'</td>
				  <td>' . $data['complain_category_name'] . '</td>
				  <td>' . $data['title'] . '</td>
				  <td>' . $data['customer_name'] . '<br>
						<i class="fa fa-envelope" style="font-size:11px;"></i> ' . $data['email'] . '
				  </td>
				  <td>' . $status . '</td>';
				  
		if($data['status'] == 'SUBMITTED') {
			
			echo '<td align="center">
				  <a href="edit_complain?id=' . $data['id_complain'] . '">EDIT</a> &nbsp;&nbsp;&nbsp;
				  <a href="#" onclick="deleteComplain(' . $data['id_complain'] . ',\'' . $data['ticket_number'] . '\')">HAPUS</a>
				  </td>';
		}
		else {
			
			echo '<td>&nbsp;</td>';
		}
		
		echo '</tr>';
		
		$i++;
	}

	echo "</tbody></table><br>";
	
	include "inc-paging.php";
?>