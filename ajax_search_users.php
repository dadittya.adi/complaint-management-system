<?php
	$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";
	
	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from tbl_user
					   where fullname like '%$keyword%' or email like '%$keyword%' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from tbl_user 
			  where fullname like '%$keyword%' or email like '%$keyword%' 
			  order by fullname ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query); 

	echo "<table class='table table-striped table-hover'>
			<thead>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='15%'>Nama</th>
				  <th width='15%'>Email</th>
				  <th width='10%'>Level</th>
				  <th width='15%'>Lihat Semua Komplain</th>
				  <th width='15%'>Email Notifikasi</th>
				  <th width='10%'>Status</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	
			</thead>
			<tbody>
			";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		if($data['view_all'] == 'Y') $view_all = 'Ya';
		else $view_all = 'Tidak';
		
		if($data['email_notification'] == 'Y') $notif = 'Ya';
		else $notif = 'Tidak';

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['fullname'] . '</td>
				  <td>' . $data['email'] . '</td>
				  <td>' . $data['level'] . '</td>
				  <td>' . $view_all . '</td>
				  <td>' . $notif . '</td>
				  <td>' . $data['status'] . '</td>
				  <td align="center">
							  <a href="#modal" onclick="getedit(' . $data['user_id'] . ')">EDIT</a> | 
							  <a href="#" onclick="deleteOperator(' . $data['user_id'] . ',\'' . $data['fullname'] . '\')">HAPUS</a>
							  </td>
				  </tr>';
		$i++;
	}

	echo "</tbody></table><br>";
	
	include "inc-paging.php";
?>