<?php
	
	//$requiredLevel = array("SUPERADMIN");
	include "check-admin-session.php";
	
	$user_id = $_SESSION['userID'];
	
	$id_complain		= sanitize_int($_REQUEST["id_complain"]);
	$solution			= sanitize_sql_string($_REQUEST["solution"]);
	
	$solution = str_replace("\r\n","<br />",$solution );
	
	if ($id_complain <> '' &&  $solution <> '' ) {
		
		//cek dulu apakah sudah solved
		$queryCheck		= "SELECT status from tbl_complain WHERE id_complain='$id_complain' and status='SOLVED' ";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "already_solved";
			exit;
		}
		
		$solved_date = date("Y-m-d H:i:s");
			
		$query = "update tbl_complain set status='SOLVED', 
				  solved_date='$solved_date', solved_by='$user_id', 
				  solution='$solution' 
				  where id_complain='$id_complain' ";
        mysqli_query($mysql_connection, $query);
		
        echo 'success'; 
		exit;  
		
	} else {
		echo "empty";
		exit;
	}
?>