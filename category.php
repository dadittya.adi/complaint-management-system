<?php 
	$requiredLevel = array("SUPERADMIN");
	include "inc-header.php";
?>

<body class="expand-data panel-data">
    
	<?php include "inc-top-bar.php"; ?>

    <?php include "inc-main-nav.php"; ?>

    <div class="pg-tp">
        <i class="fa fa-th-large"></i>
        <div class="pr-tp-inr">
            <h4>Kategori Komplain</h4>
            <span class="my_breadcrumb">
				Daftar kategori komplain customer
			</span>
        </div>
    </div>
    <!-- Page Top -->

    <div class="panel-content">
        <div class="filter-items">
            <div class="row grid-wrap">
			
				<div class="widget pad10"> 		
					<div class="col-md-12 col-sm-12 col-lg-12">
						<span id="mainNotification"></span>
					</div>
					
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-6 col-sm-6 col-lg-6">
							<input class="brd-rd5" type="text" name="keyword" id="keyword" placeholder="Kata kunci">
						  </div>
						  <div class="col-md-6 col-sm-6 col-lg-6">
							<a href="#" title="" class="brd-rd5 btn btn-sm btn-info" onclick="searchData(1);"><i class="fa fa-search"></i> Cari</a>
							<a href="#modal" title="" class="brd-rd5 btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Kategori</a>
						  </div>
						</div>
					</div>
				</div>
				
				<div class="remodal-bg"> </div>
				
				<div class="widget pad10">
					<span id="dataSpan"></span>
				</div>
				
            </div>
            <!-- Filter Items -->
        </div>
    </div>
    <!-- Panel Content -->
	
	<?php include "custom_loading.php"; ?>
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah Kategori</h4>
			<p id="modal1Desc"><span id="addNotification"></span>
			
			<div class="form-wrp">
				<div class="row">
					<div class="col-md-12">
						<label for="nama_kategori">Nama Kategori</label>
						<input type="hidden" id="id_kategori" name="id_kategori" value="0">
						<input type="text" name="nama_kategori" placeholder="Nama Kategori" class="form-control" id="nama_kategori">
					</div>
				</div>	
			</div>
			<br>
			<div class="form-wrp">
				<div class="row">
					<div class="col-md-12 text-center">
						<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
						<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
					</div>
				</div>
			</div>
			</p>
		</div>		
	</div>

	
    <?php include "inc-footer.php"; ?>

    <!-- Vendor: Javascripts -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- Vendor: Followed by our custom Javascripts -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/select2.min.js" type="text/javascript"></script>
    <script src="js/slick.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
	
	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
	<script src="js/inc-function.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_category.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {
			
			var formData = {
								'id_kategori'	: $('#id_kategori').val(),
								'nama_kategori'	: $('input[name=nama_kategori]').val()
            };


			$body.addClass("loadingClass");

			if ($("#id_kategori").val() == '0') {
				$.ajax({
					type: "POST",
					url: 'ajax_add_category.php',
					data: formData
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data kategori telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama Kategori sudah digunakan"));
					} 

				});
			} else {
				$.ajax({
					type: "POST",
					url: 'ajax_edit_category.php',
					data: formData
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data kategori telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama Kategori sudah digunakan"));
					} 

				});
			}		
		});
		
		function resetdata() {
            $("#id_kategori").val(0);
            $("#nama_kategori").val("");
            $("#modal1Title").html('Tambah Kategori');
            $("#addNotification").html("");
        }
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		function getedit(idKategori) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=kategori&id=' + idKategori,
                dataType: 'json'
            }).done(function(data) {
			
				$("#id_kategori").val(data.id);
				$("#nama_kategori").val(data.complain_category_name);
				$("#modal1Title").html('Edit Kategori');

            	$body.removeClass("loadingClass");
            });
        }
		
		function deleteKategori(idKategori,namaKategori){

        	if (confirm('Anda yakin akan menghapus kategori ' + namaKategori + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_category.php',
                    data: 'idKategori=' + idKategori
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data kategori telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "Kategori "+namaKategori+" tidak dapat dihapus"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });
	</script>
</body>

</html>