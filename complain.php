<?php 
	//$requiredLevel = array("SUPERADMIN");
	include "inc-header.php";
?>

<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

<body class="expand-data panel-data">
    
	<?php include "inc-top-bar.php"; ?>

    <?php include "inc-main-nav.php"; ?>

    <div class="pg-tp">
        <i class="fa fa-commenting-o"></i>
        <div class="pr-tp-inr">
            <h4>Data Komplain</h4>
            <span class="my_breadcrumb">
				Daftar komplain customer
			</span>
        </div>
    </div>
    <!-- Page Top -->

    <div class="panel-content">
        <div class="filter-items">
            <div class="row grid-wrap">
			
				<div class="widget pad10"> 		
					<div class="col-md-12 col-sm-12 col-lg-12">
						<span id="mainNotification"></span>
					</div>
					
					<div class="form-wrp">
						<div class="row form-group">
						  <div class="col-md-6 col-sm-12 col-lg-6">
							<div class="input-group">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal Input Komplain">
						  </div>
						  </div>
						  <div class="col-md-6 col-sm-12 col-lg-6">
							<div class="slct-bx"><span>
								<select name="src_category" id="src_category">
									<option value="0">Semua Kategori Komplain</option>
									<?php 
										$query  = 'select * from tbl_complain_category order by id ASC';
										$result = mysqli_query($mysql_connection, $query);
										while($data = mysqli_fetch_array($result)) {
											echo '<option value="'.$data['id'].'">'.$data['complain_category_name'].'</option>';
										}
									?>
								</select>
							</span></div>
						  </div>
						</div>
						<div class="row form-group">
						  <div class="col-md-6 col-sm-12 col-lg-6">
							<input class="brd-rd5" type="text" name="keyword" id="keyword" placeholder="Kata kunci">
						  </div>
						  <div class="col-md-6 col-sm-12 col-lg-6">
							<div class="slct-bx"><span>
								<select name="src_status" class="form-control" id="src_status">
									<option value=''>Semua Status</option>
									<option value='SUBMITTED'>SUBMITTED</option>
									<option value='ON PROCESS'>ON PROCESS</option>
									<option value='SOLVED'>SOLVED</option>
								</select>
							</span></div>
						  </div>
						</div>
						<div class="row form-group">
						  <div class="col-md-6 col-sm-6 col-lg-6">
							<a href="#" title="" class="brd-rd5 btn btn-sm btn-info" onclick="searchData(1);"><i class="fa fa-search"></i> Cari</a>
							<a href="add_complain" title="" class="brd-rd5 btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Komplain</a>
						  </div>
						</div>
					</div>
				</div>
				
				<div class="remodal-bg"> </div>
				
				<div class="widget pad10">
					<span id="dataSpan"></span>
				</div>
				
            </div>
            <!-- Filter Items -->
        </div>
    </div>
    <!-- Panel Content -->
	
	<?php include "custom_loading.php"; ?>
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Komplain</h4>
			<p id="modal1Desc"><span id="addNotification"></span>
			
			<input type="hidden" id="id_complain" name="id_complain" value="0">
			<input type="hidden" id="loginType" name="loginType" value="<?php echo $loginType; ?>">
			
			<div class="form-wrp">
				<div class="row">
					<div class="col-md-6">
						<label for="id_category">Tanggal Komplain</label>
						<div class="slct-bx"><span>
							<input type="text" name="submitted_date" readonly="readonly" class="form-control" id="submitted_date">
						</span></div>
					</div>
					<div class="col-md-6">
						<label for="id_category">Nomor Tiket</label>
						<div class="slct-bx"><span>
							<input type="text" name="ticket_number" readonly="readonly" class="form-control" id="ticket_number">
						</span></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label for="id_category">Kategori Komplain</label>
						<div class="slct-bx"><span>
							<input type="text" name="complain_category_name" readonly="readonly" class="form-control" id="complain_category_name">
						</span></div>
					</div>
					<div class="col-md-6">
						<label for="customer_name">Nama Customer</label>
						<input type="text" name="customer_name" readonly="readonly" class="form-control" id="customer_name">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-6">
						<label for="email">Email</label>
						<input type="text" name="email" readonly="readonly" class="form-control" id="email">
					</div>
				
					<div class="col-md-6">
						<label for="title">Judul Komplain</label>
						<input type="text" name="title" readonly="readonly" class="form-control" id="title">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<label for="message">Isi Komplain</label>
						<textarea name="message" readonly="readonly" class="form-control" id="message"></textarea>
					</div>
				</div>			

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="on_process_date">Tanggal Proses</label>
							<input type="text" name="on_process_date" readonly="readonly" class="form-control" id="on_process_date">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label for="processed_by_name">Diproses Oleh</label>
							<input type="text" name="processed_by_name" readonly="readonly" class="form-control" id="processed_by_name">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="solved_date">Tanggal Selesai Proses</label>
							<input type="text" name="solved_date" readonly="readonly" class="form-control" id="solved_date">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label for="solved_by_name">Selesai Proses Oleh</label>
							<input type="text" name="solved_by_name" readonly="readonly" class="form-control" id="solved_by_name">
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label for="solution">Solusi</label>
							<textarea name="solution" readonly="readonly" placeholder="Solusi" class="form-control" id="solution"></textarea>
						</div>
					</div>
				</div>			
				
			</div>
			<br>
			<div class="form-wrp">
				<div class="row">
					<div class="col-md-12 text-center">
						<button data-remodal-action="cancel" class="remodal-cancel">Close</button>
						<button data-remodal-action="confirm" class="remodal-confirm" id="btnSolved">Mark as Solved</button>
					</div>
				</div>
			</div>
			</p>
		</div>		
	</div>
	
	<div class="remodal" data-remodal-id="modalMedia" role="dialog" aria-labelledby="modalMediaTitle" aria-describedby="modalMediaDesc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modalMediaTitle">Gambar Komplain</h4>
			<p id="modalMediaDesc">
				<span id="spanMedia"></span>			
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Close</button>
	</div>
	
    <?php include "inc-footer.php"; ?>

    <!-- Vendor: Javascripts -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- Vendor: Followed by our custom Javascripts -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/select2.min.js" type="text/javascript"></script>
    <script src="js/slick.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
	<script src="plugins/daterangepicker/moment.min.js" type="text/javascript"></script>
	<script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	
	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
	<script src="js/inc-function.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		var currentSearchPage = 1;
		
		$('#searchDate').daterangepicker({"autoApply" : true}); 

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			if(searchPage=='0') searchPage = currentSearchPage; 
			
			var formData = {
								'searchDate'	: $('input[name=searchDate]').val(),
								'src_category'	: $('select[name=src_category]').val(),
								'src_status'	: $('select[name=src_status]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_complain.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
				currentSearchPage = searchPage;
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {
			
			if (confirm('Update status komplain menjadi solved ?')) {
			
				var formData =  {
									'id_complain'	: $('#id_complain').val(),
									'solution'		: $('textarea[name=solution]').val()
								};

				
				$body.addClass("loadingClass");

				if ($("#id_complain").val() != '0') {
					
					$.ajax({
						type: "POST",
						url: 'ajax_solve_complain.php',
						data: formData
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if (data == 'success') {
							$("#mainNotification").html(showNotification("success", "Data komplain selesai diproses"));
							modalAdd.close();
							searchData(0);
							resetdata();
							setTimeout(clearnotif, 5000);

						} else if (data == 'empty') {
							$('.remodal-bg').scrollTop(0);
							$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
						} else if (data == 'already_solved') {
							$('.remodal-bg').scrollTop(0);
							$('#addNotification').html(showNotification("error", "Data komplain sudah solved"));
						}

					});
				} 
			}
		});
		
		function resetdata() {
            $("#id_complain").val(0);
			$("#submitted_date").val('');
			$("#ticket_number").val('');
			$("#complain_category_name").val('');
			$("#customer_name").val('');
			$("#email").val('');
			$("#title").val('');
			$("#message").val('');
			$("#on_process_date").val('');
			$("#processed_by_name").val('');
			$("#solved_date").val('');
			$("#solved_by_name").val('');
			$("#solution").val('');
			$('textarea[name="solution"]').attr('readonly', false);
			$('#addNotification').html("");
        }
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		function getedit(idComplain) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=cmp&id=' + idComplain,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#id_complain").val(data.id_complain);
				$("#submitted_date").val(data.submitted_date);
				$("#ticket_number").val(data.ticket_number);
				$("#complain_category_name").val(data.complain_category_name);
				$("#customer_name").val(data.customer_name);
				$("#email").val(data.email);
				$("#title").val(data.title);
				$("#message").val(data.message);
				$("#on_process_date").val(data.on_process_date);
				$("#processed_by_name").val(data.processed_by_name);
				$("#solved_date").val(data.solved_date);
				$("#solved_by_name").val(data.solved_by_name);
				$("#solution").val(data.solution);
				
				if(data.status == 'SOLVED') $('textarea[name="solution"]').attr('readonly', true);
				else $('textarea[name="solution"]').attr('readonly', false);
				
				if($("#loginType").val() != 'SUPERADMIN') { 				
					$('textarea[name="solution"]').attr('readonly', true);
					$('#btnSolved').prop('disabled', true);
				}
				else {					
					$('textarea[name="solution"]').attr('readonly', false);
					$('#btnSolved').prop('disabled', false);
				}
				
            	$body.removeClass("loadingClass");
            });
        }
		
		function deleteComplain(id_complain,ticketNumber){

        	if (confirm('Anda yakin akan menghapus komplain dengan nomor tiket ' + ticketNumber + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_complain.php',
                    data: 'id_complain=' + id_complain
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data komplain telah dihapus"));
                    	searchData(0);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "Data komplain "+ticketNumber+" tidak dapat dihapus"));
					}
                });
            }
        }
		
		function getImages(complainID) {
			
			$body.addClass("loadingClass");

            var formData = { 'complainID' : complainID };

            $.ajax({
                type: "POST",
                url: 'ajax_search_complain_media.php',
                data: formData
            }).done(function(data) {
                $('#spanMedia').html(data);
                $body.removeClass("loadingClass");
            });
		}

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
			searchData(0);
        });
	</script>
</body>

</html>