-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2021 at 09:19 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cssdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_complain`
--

CREATE TABLE `tbl_complain` (
  `id_complain` int(11) NOT NULL,
  `ticket_number` varchar(12) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `message` text,
  `status` enum('SUBMITTED','ON PROCESS','SOLVED') DEFAULT 'SUBMITTED',
  `user_id` int(11) DEFAULT NULL,
  `submitted_date` datetime DEFAULT NULL,
  `on_process_date` datetime DEFAULT NULL,
  `processed_by` int(11) DEFAULT NULL,
  `solved_date` datetime DEFAULT NULL,
  `solved_by` int(11) DEFAULT NULL,
  `solution` text,
  `priority` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_complain`
--

INSERT INTO `tbl_complain` (`id_complain`, `ticket_number`, `id_category`, `customer_name`, `email`, `phone`, `message`, `status`, `user_id`, `submitted_date`, `on_process_date`, `processed_by`, `solved_date`, `solved_by`, `solution`, `priority`) VALUES
(7, '100221001', 4, 'Wahyu Hartono', 'w.hartono@xmail.com', '08882794385', 'Pelayanan kurang memuaskan', 'SOLVED', 1, '2021-02-10 12:49:39', '2021-02-10 15:31:09', 1, '2021-02-11 10:24:38', 1, 'oke', 1),
(9, '110221001', 1, 'Andri Ekayana', 'ekayana@xmail.com', '08175775632', 'Dokumen belum dikirim. <br />Tolong segera dikirim.<br /><br />Terima kasih', 'SUBMITTED', 1, '2021-02-11 09:56:48', NULL, NULL, NULL, NULL, NULL, 2),
(10, '110221002', 3, 'Fransiska Diah Ayu', 'fr.diahayu@xmail.com', '081234787324', 'Petugas tidak profesional. Datang tidak sesuai jadwal.', 'ON PROCESS', 1, '2021-02-11 10:27:39', '2021-02-11 10:27:45', 1, NULL, NULL, NULL, 3),
(11, '110221003', 1, 'Antonius Joko', 'antonius.joko.12@xmail.com', '088978541243', 'Dokumen tidak lengkap', 'SUBMITTED', 1, '2021-02-11 10:44:19', NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_complain_category`
--

CREATE TABLE `tbl_complain_category` (
  `id` int(11) NOT NULL,
  `complain_category_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_complain_category`
--

INSERT INTO `tbl_complain_category` (`id`, `complain_category_name`) VALUES
(1, 'Administrasi'),
(3, 'Operasional'),
(4, 'Pelayanan'),
(5, 'Lain-lain');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_complain_image`
--

CREATE TABLE `tbl_complain_image` (
  `id` int(11) NOT NULL,
  `id_complain` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_complain_image`
--

INSERT INTO `tbl_complain_image` (`id`, `id_complain`, `image`) VALUES
(16, 7, 'css_96b43afd1b301547.jpg'),
(17, 7, 'css_a9b453516a8db153.jpg'),
(20, 10, 'css_a717af25f3ec79b7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `view_all` varchar(2) DEFAULT NULL,
  `email_notification` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `email`, `password`, `fullname`, `level`, `status`, `view_all`, `email_notification`) VALUES
(1, 'superadmin@gmail.com', '0a85cae9c9123ea1acc83a866a2e9b17', 'Superadmin', 'SUPERADMIN', 'ACTIVE', 'Y', ''),
(2, 'admin1@gmail.com', '0a85cae9c9123ea1acc83a866a2e9b17', 'Admin 1', 'SUPERADMIN', 'ACTIVE', 'N', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_complain`
--
ALTER TABLE `tbl_complain`
  ADD PRIMARY KEY (`id_complain`);

--
-- Indexes for table `tbl_complain_category`
--
ALTER TABLE `tbl_complain_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_complain_image`
--
ALTER TABLE `tbl_complain_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_complain`
--
ALTER TABLE `tbl_complain`
  MODIFY `id_complain` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_complain_category`
--
ALTER TABLE `tbl_complain_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_complain_image`
--
ALTER TABLE `tbl_complain_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
