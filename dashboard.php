<?php 
	include "inc-header.php";
	
	$query 	= "select COUNT(id_complain) as jml, status 
			   from tbl_complain group by status";
	$result = mysqli_query($mysql_connection, $query);
	while($data = mysqli_fetch_array($result)) {
		
		if($data['status']=='SUBMITTED') $jml_submitted = $data['jml'];
		else if($data['status']=='ON PROCESS') $jml_onprocess = $data['jml'];
		else if($data['status']=='SOLVED') $jml_solved = $data['jml'];		
	}
?>

<body class="expand-data panel-data">
    
	<?php include "inc-top-bar.php"; ?>

    <?php include "inc-main-nav.php"; ?>

    <div class="pg-tp">
        <i class="ion-cube"></i>
        <div class="pr-tp-inr">
            <h4>Selamat Datang di <strong>Customer Support System</strong></h4>
            <span>Sistem input dan laporan komplain customer</span>
        </div>
    </div>
    <!-- Page Top -->
	
	<br>&nbsp;<br>&nbsp;

    <div class="panel-content">
        <div class="filter-items">
		
            <div class="row grid-wrap mrg20">
                <div class="col-md-4 grid-item col-sm-6 col-lg-4">
                    <div class="stat-box widget bg-clr1">
                        <i class="ion-alert-circled"></i>
                        <div class="stat-box-innr">
                            <span><?php echo number_format($jml_submitted,0,',','.'); ?></span>
                            <h5>Submitted Complain</h5>
                        </div>
                        <span>
                            <i class="ion-ios-stopwatch"></i> Updated : <?php echo date("d-m-Y H:i:s") ?></span>
                    </div>
                </div>
            
				<div class="col-md-4 grid-item col-sm-6 col-lg-4">
                    <div class="stat-box widget bg-clr4">
                        <i class="ion-android-clipboard"></i>
                        <div class="stat-box-innr">
                            <span><?php echo number_format($jml_onprocess,0,',','.'); ?></span>
                            <h5>On Process Complain</h5>
                        </div>
                        <span>
                            <i class="ion-ios-stopwatch"></i> Updated : <?php echo date("d-m-Y H:i:s") ?></span>
                    </div>
                </div>
				
				<div class="col-md-4 grid-item col-sm-6 col-lg-4">
                    <div class="stat-box widget bg-clr3">
                        <i class="ion-android-checkbox"></i>
                        <div class="stat-box-innr">
                            <span><?php echo number_format($jml_solved,0,',','.'); ?></span>
                            <h5>Solved Complain</h5>
                        </div>
                        <span>
                            <i class="ion-ios-stopwatch"></i> Updated : <?php echo date("d-m-Y H:i:s") ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Panel Content -->
    
	<?php include "inc-footer.php"; ?>

    <!-- Vendor: Javascripts -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- Vendor: Followed by our custom Javascripts -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/select2.min.js" type="text/javascript"></script>
    <script src="js/slick.min.js" type="text/javascript"></script>
	<script src="js/main.js" type="text/javascript"></script>
    
</body>

</html>