<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TblComplain extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('tbl_complain', array('id' => 'id_complain'));

        // buat kolom-kolom untuk users
        $table->addColumn('ticket_number', 'string', ['limit' => 255])
            ->addColumn('id_category', 'integer')
            ->addColumn('customer_name', 'string', ['limit' => 255])
            ->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('phone', 'string', ['limit' => 255])
            ->addColumn('message', 'text', ['limit' => 255])
            ->addColumn('status', 'string', ['limit' => 255])
            ->addColumn('user_id', 'integer')
            ->addColumn('submitted_date', 'datetime')
            ->addColumn('on_process_date', 'datetime')
            ->addColumn('processed_by', 'integer')
            ->addColumn('solved_date', 'datetime')
            ->addColumn('solved_by', 'integer')
            ->addColumn('solution', 'text')
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->save();

            $table = $this->table('tbl_complain');
            $table->addForeignKey('id_category', 'tbl_complain_category', 'id')
                ->save();

    }
}
