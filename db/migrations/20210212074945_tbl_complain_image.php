<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TblComplainImage extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('tbl_complain_image', array('id' => 'id'));

        $table->addColumn('id_complain', 'integer')
            ->addColumn('image', 'string', ['limit' => 255])
            ->save();

        $table = $this->table('tbl_complain_image');
        $table->addForeignKey('id_complain', 'tbl_complain', 'id_complain')
            ->save();


    }
}
