<?php 
	//$requiredLevel = array("SUPERADMIN");
	include "inc-header.php";
	
	$id_complain = sanitize_int($_REQUEST["id"]);

	$query 	= "select * from tbl_complain where id_complain='$id_complain'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$message= $data['message'];
	$message= str_replace("<br />", "\r\n", $message);
	$data['message'] = $message;
	
	if($loginType=='SUPERADMIN') $txt_readonly = '';
	else $txt_readonly = 'readonly="readonly"';
?>

<body class="expand-data panel-data">
    
	<?php include "inc-top-bar.php"; ?>

    <?php include "inc-main-nav.php"; ?>

    <div class="pg-tp">
        <i class="fa fa-commenting-o"></i>
        <div class="pr-tp-inr">
            <h4>Edit Komplain</h4>
            <span class="my_breadcrumb">
				Edit komplain customer
			</span>
        </div>
    </div>
    <!-- Page Top -->

    <div class="panel-content">
        <div class="filter-items">
            <div class="row grid-wrap">
			
				<div class="widget pad10"> 		
					<div class="col-md-12 col-sm-12 col-lg-12">
						<span id="mainNotification"></span>
					</div>
					
					<div class="col-md-8 col-sm-12 col-lg-8">
						<div class="form-wrp">
							<div class="row form-group">
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label>Kategori</label>
								<div class="slct-bx"><span>
									<select name="id_category" id="id_category">
										<?php 
											$queryCat  = 'select * from tbl_complain_category order by id ASC';
											$resultCat = mysqli_query($mysql_connection, $queryCat);
											while($dataCat = mysqli_fetch_array($resultCat)) {
												echo '<option value="'.$dataCat['id'].'"';
												if($data['id_category']==$dataCat['id']) echo ' selected ';
												echo '>'.$dataCat['complain_category_name'].'</option>';
											}
										?>
									</select>
								</span></div>
							  </div>
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label>Nama Customer</label>
								<input type="hidden" name="id_complain" value="<?php echo $id_complain; ?>" id="id_complain">
								<input type="text" <?php echo $txt_readonly; ?> name="customer_name" value="<?php echo $data['customer_name']; ?>" placeholder="Nama Customer" class="form-control" id="customer_name">
							  </div>
							</div>
							<div class="row form-group">
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label for="email">Email</label>
								<input type="text" <?php echo $txt_readonly; ?> name="email" value="<?php echo $data['email']; ?>" placeholder="Email" class="form-control" id="email">
							  </div>
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label>Prioritas</label>
								<div class="slct-bx"><span>
									<select name="priority" id="priority">
										<option value="1" <?php if($data['priority']=='1') echo ' selected '; ?> >HIGH</option>
										<option value="2" <?php if($data['priority']=='2') echo ' selected '; ?> >MEDIUM</option>
										<option value="3" <?php if($data['priority']=='3') echo ' selected '; ?> >LOW</option>
									</select>
								</span></div>
							  </div>
							</div>
							<div class="row form-group">
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<label for="title">Judul Komplain</label>
								<input type="text" name="title" value="<?php echo $data['title']; ?>" placeholder="Judul Komplain" class="form-control" id="title">
							  </div>  
							</div>
							<div class="row form-group">
							  <div class="col-md-12 col-sm-12 col-lg-12">
								<label for="message">Isi Komplain</label>
								<textarea name="message" placeholder="Isi Komplain" class="form-control" id="message"><?php echo $data['message']; ?></textarea>
							  </div>
							</div>
							<div class="row form-group">
								<div class="col-md-6 col-sm-12 col-lg-6">
									<div class="form-wrp">
										<div class="row form-group">
										  <div class="col-md-12 col-sm-12 col-lg-12">
											<button class="brd-rd5 btn btn-sm btn-primary" onclick="addImages(1);"><i class="fa fa-plus"></i> Tambah Gambar</button>
										  </div>
										</div>
									</div>
								</div>
							</div>
							
							<span id="spanImagesTable"></span>
							
							<div class="row form-group">
							  <div class="col-md-6 col-sm-12 col-lg-6">
								<a href="#" title="" class="brd-rd5 btn btn-sm btn-success" onclick="saveData();"><i class="fa fa-save"></i> Simpan</a>
								<a href="complain" title="" class="brd-rd5 btn btn-sm btn-danger"><i class="fa fa-remove"></i> Batal</a>
							  </div>
							</div>
						</div>
					</div>
				</div>
            </div>
            <!-- Filter Items -->
        </div>
    </div>
    <!-- Panel Content -->
	
	<?php include "custom_loading.php"; ?>
	
    <?php include "inc-footer.php"; ?>

    <!-- Vendor: Javascripts -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- Vendor: Followed by our custom Javascripts -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/select2.min.js" type="text/javascript"></script>
    <script src="js/slick.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
	
	<script src="js/inc-function.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		var rowNumber = 0;
		var rowCount  = 0;
		
		function addImages(parIdImages=0, parImage='', parImageType='NEW') {
			
			rowNumber++;
			rowCount++;
			
			var spanImagesRowName 		= 'spanImagesRow_'+rowNumber;
			var imageViewName 			= 'imageView_'+rowNumber;			
			var complainImageName 		= 'complainImage[]';
			var imagesIDName 			= 'images_id[]';
			var imagesTypeName 			= 'images_type[]';
			
			if(parImage != '') parImage = 'user_files/complain_image/'+parImage;
			else parImage = 'img/default.jpg';
			
			var input = "<span id='"+ spanImagesRowName +"'>"+
						"<input type='hidden' name='"+ imagesIDName +"' id='"+ imagesIDName +"' value='"+ parIdImages +"' />"+
						"<input type='hidden' name='"+ imagesTypeName +"' id='"+ imagesTypeName +"' value='"+ parImageType +"' />"+
						"<div class='row form-group'> "+
						"<div class='col-md-12'>"+
						"<table width='100%' class='table'>"+
							"<tr>"+
								"<td width='150px' align='center'>"+
									"<img id='"+ imageViewName +"' src='"+parImage+"' style='width: 90px;' alt='Gambar'/>"+
								"</td>"+
								"<td>"+
									"<input type='file' name='"+ complainImageName +"' id='"+ complainImageName +"' accept='image/*' onchange='readURL(this, \"#"+ imageViewName +"\")'/>"+
								"</td>"+
								"<td>"+
									"<button type='button' class='btn btn-danger btn-large' onclick='removeImages("+ rowNumber +", "+ parIdImages +");'> X </button>"+
								"</td>"+
							"</tr>"+
						"</table>"+
						"</div>"+
						"</div>"+
						"</span>";
						
			$('#spanImagesTable').append(input);
		}
		
		function removeImages(rowNum, idImages=0) {
			
			if (confirm('Hapus gambar?')) {
				
				if(idImages != 0) {					

					$body.addClass("loadingClass");
					
					var form_data = new FormData();
					form_data.append('idImages', idImages);

					$.ajax({
						type: "POST",
						url: 'ajax_delete_complain_image.php',
						processData: false,
						contentType: false,
						data: form_data
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if(data == "success") {
							$('#spanImagesRow_'+rowNum).html('');
							rowCount--;
						} 
					});
				}
				else {
					
					$('#spanImagesRow_'+rowNum).html('');
					rowCount--;
				}
			}
		}
		
		function readURL(input, view) { 
			
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result); 
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

        function saveData() {
			
			$body.addClass("loadingClass");

    		var form_data = new FormData();
          	form_data.append('id_complain', $('input[name=id_complain]').val());
			form_data.append('id_category', $('select[name=id_category]').val());
			form_data.append('customer_name', $('input[name=customer_name]').val());
			form_data.append('email', $('input[name=email]').val());
			form_data.append('title', $('input[name=title]').val());
			form_data.append('priority', $('select[name=priority]').val());
			form_data.append('message', $('textarea[name=message]').val());
			form_data.append('images_row_count', rowCount);
			
			var n = 1			
			$('input[name^="complainImage"]').each(function() {				
				var complainImageName = 'complainImage_'+n;
				form_data.append(complainImageName, $(this).prop('files')[0]);
				n++;
			});
			
			n = 1			
			$('input[name^="images_id"]').each(function() {				
				var imagesIDName = 'images_id_'+n;
				form_data.append(imagesIDName, $(this).val());
				n++;
			});
			
			n = 1			
			$('input[name^="images_type"]').each(function() {				
				var imagesTypeName = 'images_type_'+n;
				form_data.append(imagesTypeName, $(this).val());
				n++;
			});
			
			$.ajax({
				type: "POST",
                url: 'ajax_edit_complain.php',
                processData: false,
                contentType: false,
                data: form_data
			}).done(function(data) {

				data = $.trim(data); 
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data komplain sudah disimpan"));
					setTimeout(clearnotif, 5000);

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} else if (data == 'incorrect_file_type') {
					$('#mainNotification').html(showNotification("error", "File gambar harus berupa file jpg atau png atau gif"));
				} else if (data == 'invalid_email_format') {
					$('#mainNotification').html(showNotification("error", "Format email tidak benar"));
				}

				$('html, body').animate({scrollTop: '0px'}, 0);
			});
		
		}
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		<?php 
			// ambil daftar gambar 
			$query 	= "select * from tbl_complain_image where id_complain='".$id_complain."'";
			$result = mysqli_query($mysql_connection, $query);
			
			while($dataImages = mysqli_fetch_array($result)) {
				
				echo " addImages('".$dataImages['id']."', '".$dataImages['image']."', 'OLD'); ";
			}
		?>
		
	</script>
</body>

</html>