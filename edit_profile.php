<?php 
	$requiredLevel = array("SUPERADMIN");
	include "inc-header.php";
	
	$mode	= sanitize_sql_string($_REQUEST["mode"]);
	$err	= '';
	
	if($mode == 'nopass') {
		
		$fullname 	= sanitize_sql_string($_REQUEST["fullname"]);
		$email 		= sanitize_sql_string($_REQUEST["email"]);
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$err = '04';
		}
		else {
			if($fullname<>'' && $email<>'') {
				
				$user_id = $_SESSION['userID'];
				$query = "UPDATE tbl_user set fullname='$fullname', email='$email' where user_id='$user_id'";
				mysqli_query($mysql_connection, $query);
				
				$_SESSION['fullname'] 	= $fullname;
				$_SESSION['email'] 		= $email;
				$err = 'OK';
			}
			else {
				$err = '01';
			}
		}
	}
	else if($mode == 'pass') {
		
		$user_id 			= $_SESSION['userID'];
		$password 			= sanitize_sql_string($_REQUEST["current_password"]);
		$new_password 		= sanitize_sql_string($_REQUEST["new_password"]);
		$confirm_password 	= sanitize_sql_string($_REQUEST["confirm_password"]);
		
		if($password<>'' && $new_password<>'' && $confirm_password<>'') {
		
			$password 			= md5($salt.$password);
			$new_password 		= md5($salt.$new_password);
			$confirm_password 	= md5($salt.$confirm_password);
		
			//cek dulu password lama
			$query  = "select * from tbl_user where user_id='$user_id' and password='$password' ";
			$result = mysqli_query($mysql_connection, $query);
			
			if(mysqli_num_rows($result) > 0) {
				
				if($new_password == $confirm_password) {
					
					$query = "UPDATE tbl_user set password='$new_password' where user_id='$user_id'";
					mysqli_query($mysql_connection, $query);
					
					$_SESSION['password'] = $new_password;
					$err = 'OK';	
				}
				else {
					$err = '03';
				}
				
			}
			else {
				$err = '02';
			}
		}
		else {
			$err = '01';
		}
	}
?>

<body class="expand-data panel-data">
    
	<?php include "inc-top-bar.php"; ?>

    <?php include "inc-main-nav.php"; ?>

    <div class="pg-tp">
        <i class="fa fa-user"></i>
        <div class="pr-tp-inr">
            <h4>Edit Profile</h4>
            <span class="my_breadcrumb">
				Edit Profile
			</span>
        </div>
    </div>
    <!-- Page Top -->

    <div class="panel-content">
        <div class="filter-items">
            <div class="row grid-wrap">
			
				<div class="widget pad10"> 		
					<div class="col-md-12 col-sm-12 col-lg-12">
						<?php 
							
							if($err == '01') {
								
								echo "<div id='mainAlert' class='alert alert-danger alert-dismissable'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
										Data yang Anda masukkan tidak lengkap</div><br>&nbsp;";
							}
							else if($err == '02') {
								
								echo "<div id='mainAlert' class='alert alert-danger alert-dismissable'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
										Password lama tidak benar</div><br>&nbsp;";
							}
							else if($err == '03') {
								
								echo "<div id='mainAlert' class='alert alert-danger alert-dismissable'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
										Password baru dan konfirmasi password baru tidak sama</div><br>&nbsp;";
							}
							else if($err == '04') {
								
								echo "<div id='mainAlert' class='alert alert-danger alert-dismissable'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
										Format email tidak benar</div><br>&nbsp;";
							}
							else if($err == 'OK') {
								
								echo "<div id='mainAlert' class='alert alert-success alert-dismissable'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
										Data profil Anda sudah disimpan</div><br>&nbsp;";
							}
						?>
					</div>
					
					<form action="edit_profile" method="post">
					<input type="hidden" name="mode" value="nopass">
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<label>Nama Lengkap</label>
							<input class="brd-rd5" type="text" name="fullname" id="fullname" placeholder="Nama Lengkap" value="<?php echo $_SESSION['fullname']; ?>">
						  </div>
						</div>
					</div>
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<label>Email</label>
							<input class="brd-rd5" type="text" name="email" id="email" placeholder="Email" value="<?php echo $_SESSION['email']; ?>">
						  </div>
						</div>
					</div>
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<button type="submit" name="submit" class="brd-rd5 btn btn-sm btn-info">Simpan</button>
						  </div>
						</div>
					</div>					
					</form>
					
					<form action="edit_profile.php" method="post">
					<input type="hidden" name="mode" value="pass">
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<br>&nbsp;<br>
							<h4>Ubah Password</h4>
							<hr>
						  </div>
						</div>
					</div>
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<label>Password Lama</label>
							<input class="brd-rd5" type="password" name="current_password" id="current_password" placeholder="Password Lama">
						  </div>
						</div>
					</div>
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<label>Password Baru</label>
							<input class="brd-rd5" type="password" name="new_password" id="new_password" placeholder="Password Baru">
						  </div>
						</div>
					</div>
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<label>Konfirmasi Password Baru</label>
							<input class="brd-rd5" type="password" name="confirm_password" id="confirm_password" placeholder="Konfirmasi Password Baru">
						  </div>
						</div>
					</div>
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-3 col-sm-12 col-lg-3">
							<button type="submit" name="submit" class="brd-rd5 btn btn-sm btn-info">Simpan</button>
						  </div>
						</div>
					</div>					
					</form>
				</div>
				
            </div>
            <!-- Filter Items -->
        </div>
    </div>
    <!-- Panel Content -->
	
	<?php include "custom_loading.php"; ?>
	
	
    <?php include "inc-footer.php"; ?>

    <!-- Vendor: Javascripts -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- Vendor: Followed by our custom Javascripts -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/select2.min.js" type="text/javascript"></script>
    <script src="js/slick.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
	<script src="js/inc-function.js" type="text/javascript"></script>
	
</body>

</html>