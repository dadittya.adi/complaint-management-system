<?php 
	include "check-admin-session.php";
	$fullname = $_SESSION['fullname'];
	$loginType= $_SESSION['loginType'];
	$email	  = $_SESSION['email'];
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta-Information -->
    <title>Customer Support System v 1.0</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="img/favicon.png">
	
    <!-- Vendor: Bootstrap 4 Stylesheets  -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" href="css/icons.min.css" type="text/css">
    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link rel="stylesheet" href="css/responsive.css" type="text/css">

	<link rel="stylesheet" href="plugins/remodal/remodal.css">
	<link rel="stylesheet" href="plugins/remodal/remodal-default-theme.css">
	<link rel="stylesheet" href="css/jquery-ui.css">
	
    <!-- Color Scheme -->
    <link rel="stylesheet" href="css/color-schemes/color.css" type="text/css" title="color3">	
</head>