<header class="side-header expand-header">
	<div class="nav-head">Main Navigation
		<span class="menu-trigger">
			<i class="ion-android-menu"></i>
		</span>
	</div>
	<nav class="custom-scrollbar">
		<ul class="drp-sec">
			<li>
				<a href="dashboard" title="">
					<i class="fa fa-cubes"></i><span>Dashboard</span>
				</a>
			</li>
		</ul>
		
		<?php if($loginType=='SUPERADMIN') { ?>
		<ul class="drp-sec">
			<li>
				<a href="users" title="">
					<i class="fa fa-users"></i><span>Data User</span>
				</a>
			</li>
		</ul>
		<ul class="drp-sec">
			<li>
				<a href="category" title="">
					<i class="fa fa-th-large"></i><span>Kategori Komplain</span>
				</a>
			</li>
		</ul>
		<?php } ?>
		
		<ul class="drp-sec">
			<li>
				<a href="complain" title="">
					<i class="fa fa-commenting-o"></i><span>Data Komplain</span>
				</a>
			</li>
		</ul>
		
	</nav>
</header>
<!-- Side Header -->
