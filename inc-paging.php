<?php  
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	
	echo "&nbsp;";
	
	$pagination = "<div class='col-md-12 text-center'>"; 
	if($lastpage > 1) 
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<button type='button' onclick='searchData($prev);' class='btn btn-xs btn-info'>Prev</button>&nbsp;";
		else
			$pagination.= "<button type='button' class='btn btn-xs'>Prev</button>&nbsp;";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<button type='button' class='btn btn-xs'>$counter</button>&nbsp;";
				else
					$pagination.= "<button type='button' onclick='searchData($counter);' class='btn btn-xs btn-info'>$counter</button>&nbsp;";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<button type='button' class='btn btn-xs'>$counter</button>&nbsp;";
					else
						$pagination.= "<button type='button' onclick='searchData($counter);' class='btn btn-xs  btn-info'>$counter</button>&nbsp;";					
				}
				$pagination.= "<button type='button' class='btn btn-xs'>...</button>&nbsp;";
				$pagination.= "<button type='button' onclick='searchData($lpm1);' class='btn btn-xs  btn-info'>$lpm1</button>&nbsp;";
				$pagination.= "<button type='button' onclick='searchData($lastpage);' class='btn btn-xs  btn-info'>$lastpage</button>&nbsp;";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<button type='button' onclick='searchData(1);' class='btn btn-xs  btn-info'>1</button>&nbsp;";
				$pagination.= "<button type='button' onclick='searchData(2);' class='btn btn-xs  btn-info'>2</button>&nbsp;";
				$pagination.= "<button type='button' class='btn btn-xs'>...</button>&nbsp;";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<button type='button' class='btn btn-xs'>$counter</button>&nbsp;";
					else
						$pagination.= "<button type='button' onclick='searchData($counter);' class='btn btn-xs  btn-info'>$counter</button>&nbsp;";					
				}
				$pagination.= "<button type='button' class='btn btn-xs'>...</button>&nbsp;";
				$pagination.= "<button type='button' onclick='searchData($lmp1);' class='btn btn-xs  btn-info'>$lmp1</button>&nbsp;";
				$pagination.= "<button type='button' onclick='searchData($lastpage);' class='btn btn-xs  btn-info'>$lastpage</button>&nbsp;";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<button type='button' onclick='searchData(1);' class='btn btn-xs  btn-info'>1</button>&nbsp;";
				$pagination.= "<button type='button' onclick='searchData(2);' class='btn btn-xs  btn-info'>2</button>&nbsp;";
				$pagination.= "<button type='button' class='btn btn-xs'>...</button>&nbsp;";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<button type='button' class='btn btn-xs'>$counter</button>&nbsp;";
					else
						$pagination.= "<button type='button' onclick='searchData($counter);' class='btn btn-xs  btn-info'>$counter</button>&nbsp;";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<button type='button' onclick='searchData($next);' class='btn btn-xs  btn-info'>Next</button>&nbsp;";
		else
			$pagination.= "<button type='button' class='btn btn-xs'>Next</button>&nbsp;";
		$pagination.= "</div>\n";		
	}
	$pagination .= "</div>";
	
	echo $pagination;
?>