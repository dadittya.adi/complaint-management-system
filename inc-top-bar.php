<div class="topbar">
        <div class="logo">
            <h1>
                <a href="#" title="">
                    <img src="img/logoWSU.png" alt="" style="width:340px;"/>
                </a>
            </h1>
        </div>
        <div class="topbar-data">
            <div class="usr-act">
                <span>
					<span class="username_info">Selamat Datang, <?php echo $fullname; ?></span>
                    <img src="images/resource/topbar-usr1.png" alt="" />
                    <i class="sts online"></i>
                </span>
                <div class="usr-inf">
                    <div class="usr-thmb brd-rd50">
                        <img class="brd-rd50" src="images/resource/usr.png" alt="" />
                        <i class="sts online"></i>
                    </div>
                    <h5>
                        <a href="#" title=""><?php echo $fullname; ?></a>
                    </h5>
                    <span><a href="edit_profile">Edit Profile</a></span>
                    <br>
                    <div class="usr-ft">
                        <a class="btn-danger" href="logout" title="logout">
                            <i class="fa fa-sign-out"></i> Logout
						</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="topbar-bottom-colors">
            <i style="background-color: #2c3e50;"></i>
            <i style="background-color: #9857b2;"></i>
            <i style="background-color: #2c81ba;"></i>
            <i style="background-color: #5dc12e;"></i>
            <i style="background-color: #feb506;"></i>
            <i style="background-color: #e17c21;"></i>
            <i style="background-color: #bc382a;"></i>
        </div>
    </div>
    <!-- Topbar -->