<?php 
	ini_set("display_errors","0"); 
	error_reporting(0); 
	$err = $_REQUEST['e'];
  include "inc-db.php";
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta-Information -->
  <title>Customer Support System v 1.0</title>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="img/favicon.png">

  <!-- Vendor: Bootstrap 4 Stylesheets  -->
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

  <!-- Our Website CSS Styles -->
  <link rel="stylesheet" href="css/icons.min.css" type="text/css">
  <link rel="stylesheet" href="css/main.css" type="text/css">
  <link rel="stylesheet" href="css/responsive.css" type="text/css">

  <!-- Color Scheme -->
  <link rel="stylesheet" href="css/color-schemes/color.css" type="text/css" title="color3">
</head>

<body class="expand-data panel-data">
  
  <div class="panel-content">
    <div class="lgn-wrp grysh">
      <div class="bg-img" style="background-image: url(images/blurred.jpg);"></div>
      <div class="lgn-innr">
        <div class="widget lgn-frm">
          <div class="frm-tl">
			<img src="img/logoWSU.png" style="width:100%;">
			<br><br>
            <h4>Login</h4>
			<?php 
				if($err == '01') {
					echo '<br><span style="color:red;">Silahkan masukkan username dan password</span>';
				}
				else if($err == '02') {
					echo '<br><span style="color:red;">Login gagal, periksa kembali username dan password Anda</span>';
				}
				else if($err == '03') {
					echo '<br><span style="color:red;">Silahkan login terlebih dahulu</span>';
				}
				else if($err == '04') {
					echo '<br><span style="color:red;">Invalid captcha response</span>';
				}
			?>
          </div>
          <form method="post" action="login_process.php">
            <div class="row mrg10">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <input class="brd-rd5" type="text" name="email" placeholder="Email" required />
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <input class="brd-rd5" type="password" name="password" placeholder="Password" required />
              </div>
              <div class="col-md-12 col-sm-12 col-lg-12">
                <button class="green-bg brd-rd5" type="submit">LOGIN</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <footer>
        <p>Copyright &copy; <?php echo date('Y'); ?>
		  <a href="#" title="">Widya Solusi Utama</a></p>
		<span>version 1.0</span>
      </footer>
    </div>
    <!-- Login Wrapper -->
  </div>
  <!-- Panel Content -->
  <footer>
    <p>Copyright &copy; <?php echo date('Y'); ?>
      <a href="#" title="">Widya Solusi Utama</a></p>
    <span>version 1.0</span>
  </footer>


  <!-- Vendor: Javascripts -->
  <script src="js/jquery.min.js" type="text/javascript"></script>
  <!-- Vendor: Followed by our custom Javascripts -->
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script src="js/select2.min.js" type="text/javascript"></script>
  <script src="js/slick.min.js" type="text/javascript"></script>

  <!-- Our Website Javascripts -->
  <!-- script src="js/isotope.min.js" type="text/javascript"></script>
  <script src="js/isotope-int.js" type="text/javascript"></script>
  <script src="js/jquery.counterup.js" type="text/javascript"></script>
  <script src="js/waypoints.min.js" type="text/javascript"></script>
  <script src="js/highcharts.js" type="text/javascript"></script>
  <script src="js/exporting.js" type="text/javascript"></script>
  <script src="js/highcharts-more.js" type="text/javascript"></script>
  <script src="js/moment.min.js" type="text/javascript"></script>
  <script src="js/jquery.circliful.min.js" type="text/javascript"></script>
  <script src="js/fullcalendar.min.js" type="text/javascript"></script>
  <script src="js/jquery.downCount.js" type="text/javascript"></script>
  <script src="js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
  <script src="js/jquery.formtowizard.js" type="text/javascript"></script>
  <script src="js/form-validator.min.js" type="text/javascript"></script>
  <script src="js/form-validator-lang-en.min.js" type="text/javascript"></script>
  <script src="js/cropbox-min.js" type="text/javascript"></script>
  <script src="js/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="js/ion.rangeSlider.min.js" type="text/javascript"></script>
  <script src="js/jquery.poptrox.min.js" type="text/javascript"></script>
  <script src="js/styleswitcher.js" type="text/javascript"></script -->
  <script src="js/main.js" type="text/javascript"></script>
</body>

</html>