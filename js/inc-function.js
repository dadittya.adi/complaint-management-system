function showNotification(dialogType, message) {

    var iconType = "fa-info";

    if (dialogType == 'error') {
        dialogType = "danger";
        iconType = "fa-ban";
    }
    else if (dialogType == 'info') {
        dialogType = "info";
        iconType = "fa-info";
    }
    else if (dialogType == 'warning') {
        dialogType = "warning";
        iconType = "fa-warning";
    }
    else if (dialogType == 'success') {
        dialogType = "success";
        iconType = "fa-check";
    }

    /*var dialog = "<div class='alert alert-" + dialogType + " alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><h4><i class='icon fa " + iconType + "'></i> Notification</h4>" + message + "</div>";*/
	var dialog = "<div id='mainAlert' class='alert alert-" + dialogType + " alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" + message + "</div><br>&nbsp;";
    return dialog;
}
function onlynumber(id) {
    $(document).ready(function() {
        $("#" + id).keydown(function(event) {
        	
        	// Allow only backspace and delete and tab
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
                // let it happen, don't do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.keyCode < 48 || event.keyCode > 57) {
                    
                    if (event.keyCode == 190) {

                    } else {
                        event.preventDefault();
                    }
                }

            }
        });
    });
}

function showLoadingModal() {
	loadingModal.open();
}

function hideLoadingModal() {
	
	if(loadingModal.getState()=='opened') { 
		loadingModal.close();
	}
	else {
		setTimeout("hideLoadingModal()", 300);
	}
}
