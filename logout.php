<?php 
	session_start();
	unset($_SESSION['loginType']);
	unset($_SESSION['fullname']);
	unset($_SESSION['username']);
	unset($_SESSION['password']);
	unset($_SESSION['userID']);

	session_destroy();

	header("location:index.php");
	exit;
?>