<?php 
	$requiredLevel = array("SUPERADMIN");
	include "inc-header.php";
?>

<body class="expand-data panel-data">
    
	<?php include "inc-top-bar.php"; ?>

    <?php include "inc-main-nav.php"; ?>

    <div class="pg-tp">
        <i class="fa fa-users"></i>
        <div class="pr-tp-inr">
            <h4>Data User</h4>
            <span class="my_breadcrumb">
				Daftar login user
			</span>
        </div>
    </div>
    <!-- Page Top -->

    <div class="panel-content">
        <div class="filter-items">
            <div class="row grid-wrap">
			
				<div class="widget pad10"> 		
					<div class="col-md-12 col-sm-12 col-lg-12">
						<span id="mainNotification"></span>
					</div>
					
					<div class="form-wrp">
						<div class="row">
						  <div class="col-md-6 col-sm-6 col-lg-6">
							<input class="brd-rd5" type="text" name="keyword" id="keyword" placeholder="Kata kunci">
						  </div>
						  <div class="col-md-6 col-sm-6 col-lg-6">
							<a href="#" title="" class="brd-rd5 btn btn-sm btn-info" onclick="searchData(1);"><i class="fa fa-search"></i> Cari</a>
							<a href="#modal" title="" class="brd-rd5 btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah User</a>
						  </div>
						</div>
					</div>
				</div>
				
				<div class="remodal-bg"> </div>
				
				<div class="widget pad10">
					<span id="dataSpan"></span>
				</div>
				
            </div>
            <!-- Filter Items -->
        </div>
    </div>
    <!-- Panel Content -->
	
	<?php include "custom_loading.php"; ?>
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah User</h4>
			<p id="modal1Desc"><span id="addNotification"></span>
			
			<div class="form-wrp">
				<div class="row">
					<div class="col-md-6">
						<label for="fullname">Nama</label>
						<input type="hidden" id="operator_id" name="operator_id" value="0">
						<input type="text" name="fullname" placeholder="Nama" class="form-control" id="fullname">
					</div>

					<div class="col-md-6">
						<label for="email">Email</label>
						<input type="text" name="email" placeholder="Email" class="form-control" id="email">
					</div>
				</div>	

				<div class="row">
					<div class="col-md-6">
						<label for="password">Password <span id="passwdNotif"></span></label>
						<input type="password" name="password" placeholder="Password" class="form-control" id="password">
					</div>

					<div class="col-md-6">
						<label for="passwordConfirm">Konfirmasi Password <span id="passwdNotifConfirm"></span></label>
						<input type="password" name="passwordConfirm" placeholder="Konfirmasi Password" class="form-control" id="passwordConfirm">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<span id="passwdLabel" style="font-size:11px;"></span>
					</div>
				</div>
				
				<div class="row">
				
					<div class="col-md-6">
						<label for="level">Level</label>
						<div class="slct-bx"><span>
							<select name="level" class="form-control" id="level">
								<option value="SUPERADMIN">SUPERADMIN</option>
								<option value="USER">USER</option>
							</select>
						</span></div>
					</div>
					<div class="col-md-6">
						<label for="status">Status</label>
						<div class="slct-bx"><span>
							<select name="status" class="form-control" id="status">
								<option value="ACTIVE">ACTIVE</option>
								<option value="NOT ACTIVE">NOT ACTIVE</option>
							</select>
						</span></div>
					</div>
				</div>
				
				<div class="row">
				
					<div class="col-md-6">
						<label for="view_all">Lihat Semua Data Komplain</label>
						<div class="slct-bx"><span>
							<select name="view_all" id="view_all">
								<option value="Y">Ya</option>
								<option value="N">Tidak</option>
							</select>
						</span></div>
					</div>
				
					<div class="col-md-6">
						<label for="email_notification">Email Notifikasi Komplain</label>
						<div class="slct-bx"><span>
							<select name="email_notification" id="email_notification">
								<option value="Y">Ya</option>
								<option value="N">Tidak</option>
							</select>
						</span></div>
					</div>
					
				</div>
				
				<div class="row">
					<div class="col-md-12">&nbsp;</div>
				</div>
			</div>
			<br>
			<div class="form-wrp" style="margin-top:15px;">
				<div class="row">
					<div class="col-md-12 text-center">
						<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
						<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
					</div>
				</div>
			</div>
			</p>
		</div>		
	</div>
	
    <?php include "inc-footer.php"; ?>

    <!-- Vendor: Javascripts -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!-- Vendor: Followed by our custom Javascripts -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/select2.min.js" type="text/javascript"></script>
    <script src="js/slick.min.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
	
	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
	<script src="js/inc-function.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_users.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {
			
			var formData = {
								'operator_id'				: $('#operator_id').val(),
								'operator_name'				: $('input[name=fullname]').val(),
								'operator_email'			: $('input[name=email]').val(),
								'operator_password'			: $('input[name=password]').val(),
								'operator_passwordConfirm'	: $('input[name=passwordConfirm]').val(),
								'operator_level'			: $('select[name=level]').val(),
								'view_all'					: $('select[name=view_all]').val(),
								'operator_status'			: $('select[name=status]').val(),
								'email_notification'		: $('select[name=email_notification]').val()
            };

			if($("#password").val() != $("#passwordConfirm").val()) {
				$('#addNotification').html(showNotification("error", "Password dan konfirmasi password tidak sama"));
			}
			else {

				$body.addClass("loadingClass");

				if ($("#operator_id").val() == '0') {
					$.ajax({
						type: "POST",
						url: 'ajax_add_users.php',
						data: formData
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if (data == 'success') {
							$("#mainNotification").html(showNotification("success", "Data user telah disimpan"));
							modalAdd.close();
							searchData(1);
							$('#dataSpan').html(data);
							resetdata();
							setTimeout(clearnotif, 5000);

						} else if (data == 'empty') {
							$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
						} else if (data == 'email_exist') {
							$('#addNotification').html(showNotification("error", "Email sudah digunakan"));
						} else if (data == 'password_not_match') {
							$('#addNotification').html(showNotification("error", "Password dan konfirmasi password tidak sama"));
						} else if (data == 'invalid_email_format') {
							$('#addNotification').html(showNotification("error", "Format email tidak benar"));
						}

					});
				} else {
					$.ajax({
						type: "POST",
						url: 'ajax_edit_users.php',
						data: formData
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if (data == 'success') {
							$("#mainNotification").html(showNotification("success", "Data user telah disimpan"));
							modalAdd.close();
							searchData(1);
							$('#dataSpan').html(data);
							resetdata();
							setTimeout(clearnotif, 5000);

						} else if (data == 'empty') {
							$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
						} else if (data == 'email_exist') {
							$('#addNotification').html(showNotification("error", "Email sudah digunakan"));
						} else if (data == 'password_not_match') {
							$('#addNotification').html(showNotification("error", "Password dan konfirmasi password tidak sama"));
						} else if (data == 'invalid_email_format') {
							$('#addNotification').html(showNotification("error", "Format email tidak benar"));
						}

					});
				}
			}
		
		});
		
		function resetdata() {
            $("#operator_id").val(0);
            $("#fullname").val("");
			$("#email").val("");
			$("#password").val("");
			$("#passwordConfirm").val("");
			$("#level").val($("#level option:first").val());
			$('#level').val($("#level option:first").val()).trigger('change.select2');
			$("#view_all").val($("#view_all option:first").val());
			$('#view_all').val($("#view_all option:first").val()).trigger('change.select2');
			$("#status").val($("#status option:first").val());
			$('#status').val($("#status option:first").val()).trigger('change.select2');
			$("#email_notification").val($("#email_notification option:first").val());
			$('#email_notification').val($("#email_notification option:first").val()).trigger('change.select2');
            $("#modal1Title").html('Tambah User');
            $("#addNotification").html("");
			$("#passwdNotif").html("");
			$("#passwdNotifConfirm").html("");
			$("#passwdLabel").html("");
        }
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		function getedit(operatorID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=adm&id=' + operatorID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#operator_id").val(data.user_id);
				$("#status").val(data.status);
				$('#status').val(data.status).trigger('change.select2');
				$("#level").val(data.level);
				$('#level').val(data.level).trigger('change.select2');
				$("#view_all").val(data.view_all);
				$('#view_all').val(data.view_all).trigger('change.select2');
				$("#email_notification").val(data.email_notification);
				$('#email_notification').val(data.email_notification).trigger('change.select2');
				$("#email").val(data.email);
				$("#fullname").val(data.fullname);
				$("#modal1Title").html('Edit User');
				$("#passwdNotif").html("<font color='red'><i>*</i></font>");
				$("#passwdNotifConfirm").html("<font color='red'><i>*</i></font>");
				$("#passwdLabel").html("<font color='red'><i>* Biarkan kosong jika tidak akan mengubah password</i></font><br>&nbsp;");

            	$body.removeClass("loadingClass");
            });
        }
		
		function deleteOperator(operatorID,operatorName){

        	if (confirm('Anda yakin akan menghapus user ' + operatorName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_users.php',
                    data: 'operatorID=' + operatorID
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data user telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "User "+operatorName+" tidak dapat dihapus"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });
	</script>
</body>

</html>